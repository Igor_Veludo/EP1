#include "filtroNegativo.hpp"

FiltroNegativo :: FiltroNegativo(){

}

FiltroNegativo :: ~FiltroNegativo(){

}

list<RGB> FiltroNegativo::aplicaFiltro(unsigned int escalamaxima, list<RGB> rgb){
	unsigned char max = escalamaxima;
	list<RGB>::iterator i;
	list<RGB> temp;

	for(i = rgb.begin(); i != rgb.end(); i++){
		pixels = *i;

		pixels.setR(max - pixels.getR());
		pixels.setG(max - pixels.getG());
		pixels.setB(max - pixels.getB());

		temp.push_back(pixels);
	}
	return temp;

}