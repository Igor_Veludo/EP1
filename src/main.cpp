#include "imagem.hpp"
#include "filtroNegativo.hpp"
#include "filtroPolarizado.hpp"
#include "filtroPretoBranco.hpp"

int main(){
	Imagem* imagem = new Imagem();
	FiltroNegativo* filtronegativo = new FiltroNegativo();
	FiltroPolarizado* filtropolarizado = new FiltroPolarizado();
	FiltroPretoBranco* filtropretobranco = new FiltroPretoBranco();

	int escolha;
	ifstream arquivo;
	ofstream novoArquivo;

	do{
		cout << "Opções:\n1-Negativo\n2-Polarizado\n3-Preto e Branco\n0-Sair" << endl;;
		cin >> escolha;

		switch(escolha){
			case 0:
				cout << "Fechando..." << endl;
				exit(0);
			break;

			case 1:
				imagem->abreImagem(&arquivo);
				imagem->coletaDados(&arquivo);
				imagem->setRGB(filtronegativo->aplicaFiltro(imagem->getEscalaMaxima(), imagem->getRGB()));
				imagem->novaImagem(&novoArquivo);
				imagem->geraImagem(&novoArquivo);

			break;

			case 2:	
				imagem->abreImagem(&arquivo);
				imagem->coletaDados(&arquivo);
				imagem->setRGB(filtropolarizado->aplicaFiltro(imagem->getEscalaMaxima(), imagem->getRGB()));
				imagem->novaImagem(&novoArquivo);
				imagem->geraImagem(&novoArquivo);
			break;

			case 3:
				imagem->abreImagem(&arquivo);
				imagem->coletaDados(&arquivo);
				imagem->setRGB(filtropretobranco->aplicaFiltro(imagem->getRGB()));
				imagem->novaImagem(&novoArquivo);
				imagem->geraImagem(&novoArquivo);
			break;
			
			default:
				cout << "Opção incorreta..." << endl;
			break;
		}
	}while(escolha != 0);
	
	return 0;
}