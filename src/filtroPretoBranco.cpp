#include "filtroPretoBranco.hpp"

FiltroPretoBranco :: FiltroPretoBranco(){

}

FiltroPretoBranco :: ~FiltroPretoBranco(){

}

list<RGB> FiltroPretoBranco::aplicaFiltro(list<RGB> rgb){
	list<RGB>::iterator i;
	list<RGB> temp;
	unsigned char novoValor;

	for(i = rgb.begin(); i != rgb.end(); i++){
		pixels = *i;

		novoValor = (0.299 * (pixels.getR())) + (0.587 * (pixels.getG())) + (0.144 * (pixels.getB()));

		pixels.setR(novoValor);
		pixels.setG(novoValor);
		pixels.setB(novoValor);

		temp.push_back(pixels);
	}
	return temp;

}