#include "imagem.hpp"
#include "rgb.hpp"

Imagem :: Imagem(){

}

Imagem :: ~Imagem(){

}

string Imagem :: getFormato(){
	return formato;
}

int Imagem :: getAltura(){
	return altura;
}

int Imagem :: getLargura(){
	return largura;
}

int Imagem :: getEscalaMaxima(){
	return escalamaxima;
}

void Imagem :: setFormato(string formato){
	this->formato = formato;
}


void Imagem :: setAltura(int altura){
	this->altura = altura;
}

void Imagem :: setLargura(int largura){
	this->largura = largura;
}

void Imagem :: setEscalaMaxima(unsigned int escalamaxima){
	this->escalamaxima = escalamaxima;
}

list<RGB> Imagem :: getRGB(){
	return rgb;
}

void Imagem :: setRGB(list<RGB> rgb){
	this->rgb = rgb;
}

void Imagem :: abreImagem(ifstream* arquivo){
	string nome;

	do{
		cout << "Insira o nome da imagem: ";
		cin >> nome;

		nome = "./doc/" + nome + ".ppm";
		arquivo->open(nome.c_str());

		if(arquivo->fail()){
			cout << "Erro na abertura do arquivo!" << endl;
		}
	}while(arquivo->fail());
}

void Imagem :: novaImagem(ofstream* novoArquivo){
	string nome;

	cout << "Insira um nome para imagem de saida: " << endl;
	cin >> nome;

	nome = "./doc/" + nome + ".ppm";

	novoArquivo->open(nome.c_str());
}

void Imagem :: coletaDados(ifstream* arquivo){
	unsigned char aux;
	RGB* cores = new RGB();

	testeComentario(arquivo);
	(*arquivo) >> formato;
	testeComentario(arquivo);
	(*arquivo) >> altura;
	(*arquivo) >> largura;
	testeComentario(arquivo);
	(*arquivo) >> escalamaxima;

	while(!arquivo->eof()){
		cores->setR(aux = arquivo->get());
		cores->setG(aux = arquivo->get());
		cores->setB(aux = arquivo->get());
		
		rgb.push_back((*cores));
	}

	arquivo->close();
}

void Imagem :: testeComentario(ifstream* arquivo){
	string linha;
	(*arquivo) >> linha;
	
	if(linha[0] != '#'){
		arquivo->seekg(-linha.size(),ios_base::cur);
		return;
	}

	while(linha[0] == '#'){
		getline(*arquivo,linha);
		(*arquivo) >> linha;
	}
	arquivo->seekg(-linha.size(),ios_base::cur);
}

void Imagem :: geraImagem(ofstream* novoArquivo){
	RGB cor;
	list<RGB>::iterator i;

	(*novoArquivo) << getFormato() << endl;
	(*novoArquivo) << getAltura() << " " << getLargura() << endl;
	(*novoArquivo) << getEscalaMaxima() << endl;

	for(i=rgb.begin(); i != rgb.end(); i++){
		cor = *i;

		novoArquivo->put(cor.getR());
		novoArquivo->put(cor.getG());
		novoArquivo->put(cor.getB());
	}

	novoArquivo->close();
	cout << "\nProcesso concluido!" << endl;
}