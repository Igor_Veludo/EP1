#include "rgb.hpp"

RGB::RGB(){

}

RGB::~RGB(){
	
}

unsigned char RGB::getR(){
	return r;
}

unsigned char RGB::getG(){
	return g;
}

unsigned char RGB::getB(){
	return b;
}

void RGB::setR(unsigned char r){
	this->r = r;
}

void RGB::setG(unsigned char g){
	this->g = g;
}

void RGB::setB(unsigned char b){
	this->b = b;
}