#include "filtroPolarizado.hpp"

FiltroPolarizado::FiltroPolarizado(){

}

FiltroPolarizado::~FiltroPolarizado(){

}

list<RGB> FiltroPolarizado::aplicaFiltro(unsigned int escalamaxima, list<RGB> rgb ){
    unsigned char max = escalamaxima;
    list<RGB>::iterator i;
    list<RGB> temp;

    for(i = rgb.begin(); i != rgb.end(); i++){
        pixels = *i;
        
        if(pixels.getR() < max/2){
                pixels.setR(0);
        }else{
                pixels.setR(max);
        }

        if(pixels.getG() < max/2){
                pixels.setG(0);
        }else{
                pixels.setG(max);
        }

        if(pixels.getB() < max/2){
                pixels.setB(0);
        }else{
                pixels.setB(max);
        }              

        temp.push_back(pixels);
    }
    return temp;
}