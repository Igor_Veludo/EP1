#ifndef RGB_HPP
#define RGB_HPP

#include <iostream>

using namespace std;

class RGB {
private:
	unsigned char r;
	unsigned char g;
	unsigned char b;
public:
	RGB();
	~RGB();

	void setR(unsigned char r);
	unsigned char getR();
	void setG(unsigned char g);
	unsigned char getG();
	void setB(unsigned char b);
	unsigned char getB();
};
#endif