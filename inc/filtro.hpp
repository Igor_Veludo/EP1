#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <string>
#include <iostream>
#include <list>

#include "imagem.hpp"
#include "rgb.hpp"

using namespace std;

class Filtro{
	protected:
		class RGB pixels;
	public:
		Filtro();
		~Filtro();

		void setPixels(class RGB pixels);
		class RGB getPixels();
		list<RGB> aplicaFiltro();
};
#endif