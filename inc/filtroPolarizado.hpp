#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "filtro.hpp"

using namespace std;

class FiltroPolarizado : public Filtro{
	private:

	public:
		FiltroPolarizado();
		~FiltroPolarizado();

		list<RGB> aplicaFiltro(unsigned int escalamaxima, list<RGB> rgb);
};
#endif