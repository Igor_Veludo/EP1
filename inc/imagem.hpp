#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <list>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include "rgb.hpp"

using namespace std;

class Imagem{
	private:
		string formato;
		int altura;
		int largura;
		unsigned int escalamaxima;
		list<RGB> rgb;
	public:
		Imagem();
		~Imagem();

		void abreImagem(ifstream* arquivo);
		void coletaDados(ifstream* arquivo);
		void testeComentario(ifstream* arquivo);
		void novaImagem(ofstream* novoArquivo);
		void geraImagem(ofstream* novoArquivo);

		string getFormato();
		int getAltura();
		int getLargura();
		int getEscalaMaxima();

		void setFormato(string formato);
		void setAltura(int altura);
		void setLargura(int largura);
		void setEscalaMaxima(unsigned int escalamaxima);

		list<RGB> getRGB();
		void setRGB(list<RGB> rgb);

};
#endif