#ifndef FILTROPRETOBRANCO_HPP
#define FILTROPRETOBRANCO_HPP

#include "filtro.hpp"

using namespace std;

class FiltroPretoBranco : public Filtro{
	private:

	public:
		FiltroPretoBranco();
		~FiltroPretoBranco();

		list<RGB> aplicaFiltro(list<RGB> rgb);
};
#endif