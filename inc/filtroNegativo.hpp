#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "filtro.hpp"
using namespace std;

class FiltroNegativo : public Filtro{
	private:

	public:
		FiltroNegativo();
		~FiltroNegativo();

		list<RGB> aplicaFiltro(unsigned int escalamaxima, list<RGB> rgb);
};
#endif