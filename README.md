# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Para compilar
	$make
## Para executar
	$make run
## Para limpar
	$make clean